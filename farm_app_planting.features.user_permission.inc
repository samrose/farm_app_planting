<?php
/**
 * @file
 * farm_app_planting.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function farm_app_planting_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create planting content'.
  $permissions['create planting content'] = array(
    'name' => 'create planting content',
    'roles' => array(
      'administrator' => 'administrator',
      'farmer' => 'farmer',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any planting content'.
  $permissions['delete any planting content'] = array(
    'name' => 'delete any planting content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own planting content'.
  $permissions['delete own planting content'] = array(
    'name' => 'delete own planting content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any planting content'.
  $permissions['edit any planting content'] = array(
    'name' => 'edit any planting content',
    'roles' => array(
      'administrator' => 'administrator',
      'farmer' => 'farmer',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own planting content'.
  $permissions['edit own planting content'] = array(
    'name' => 'edit own planting content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  return $permissions;
}
